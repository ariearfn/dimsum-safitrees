export default defineNuxtConfig({
  app: {
    head: {
      charset: "utf-16",
      viewport: "width=500, initial-scale=1",
      titleTemplate: "%s Dimsum Safitrees",
      meta: [
        {
          name: "Title",
          content: "Idunno",
        },
      ],
      link: [{ rel: "icon", type: "image/x-icon", href: "/img/cry.png" }],
    },
  },
  devtools: { enabled: true },
  modules: ["@nuxt/image", "@nuxt/ui", "@nuxtjs/device", "@nuxtjs/supabase"],
  image: {
    format: ["webp"],
  },
  build: {
    transpile: ["@heroicons/vue"],
  },
  tailwindcss: {
    configPath: "tailwind.config.js",
    exposeConfig: false,
    config: {},
    injectPosition: 0,
    viewer: true,
  },
  supabase: {
    redirect: false,
    // redirectOptions: {
    //   login: "/login",
    //   callback: "/confirm",
    //   exclude: [],
    // },
  },
});
